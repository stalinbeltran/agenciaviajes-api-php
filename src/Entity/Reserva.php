<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservaRepository")
 */
class Reserva
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Viajero", inversedBy="reservas")
     * @ORM\JoinColumn(nullable=false)
     */
    
 
    private $viajero;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Viaje", inversedBy="reservas")
     * @ORM\JoinColumn(nullable=false)
     */
    

    private $viaje;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajero(): ?Viajero
    {
        return $this->viajero;
    }

    public function setViajero(?Viajero $viajero): self
    {
        $this->viajero = $viajero;

        return $this;
    }

    public function getViaje(): ?Viaje
    {
        return $this->viaje;
    }

    public function setViaje(?Viaje $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }
}
