<?php

namespace App\Repository;

use App\Entity\Viajero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Viajero|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajero|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajero[]    findAll()
 * @method Viajero[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeroRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Viajero::class);
    }

    // /**
    //  * @return Viajero[] Returns an array of Viajero objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Viajero
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
