<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190517001516 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE reserva (id INT AUTO_INCREMENT NOT NULL, viajero_id INT NOT NULL, viaje_id INT NOT NULL, INDEX IDX_188D2E3BFAF4CFE9 (viajero_id), INDEX IDX_188D2E3B94E1E648 (viaje_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reserva ADD CONSTRAINT FK_188D2E3BFAF4CFE9 FOREIGN KEY (viajero_id) REFERENCES viajero (id)');
        $this->addSql('ALTER TABLE reserva ADD CONSTRAINT FK_188D2E3B94E1E648 FOREIGN KEY (viaje_id) REFERENCES viaje (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE reserva');
    }
}
