<?php

namespace App\Controller;

use App\Entity\Viajero;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ViajeroController extends AbstractController
{

    
    /**
     * @Route("/viajero/{id}", name="viajero_show")
     */
    public function show(Viajero $viajero)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders); 
        $jsonContent = $serializer->serialize($viajero, 'json');
        return new Response($jsonContent);
    }












    /**
     * @Route("/viajero/edit/{id}", methods={"POST","HEAD"})
     */
    public function update(Request $request, $id)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        
        $serializer = new Serializer($normalizers, $encoders);  
        $data = $request->getContent();

        $viajeroEnviado = $serializer->deserialize($data, Viajero::class, 'json');
        $entityManager = $this->getDoctrine()->getManager();
        $viajero = $entityManager->getRepository(Viajero::class)->find($id);

        if (!$viajero) {
            throw $this->createNotFoundException(
                'No viajero found for id '.$id
            );
        }

        $viajero->setCedula($viajeroEnviado->getCedula());
        $viajero->setNombre($viajeroEnviado->getNombre());
        $viajero->setDireccion($viajeroEnviado->getDireccion());
        $viajero->setTelefono($viajeroEnviado->getTelefono());
        $entityManager->flush();

        $jsonContent = $serializer->serialize($viajero, 'json');
        return new Response($jsonContent);
    }









    /**
     * @Route("/viajero/new/", methods={"POST","HEAD"})
     */
    public function new(SerializerInterface $serializer, Request $request)
    {
        $data = $request->getContent();
        $viajeroEnviado = $serializer->deserialize($data, Viajero::class, 'json');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($viajeroEnviado);
        $entityManager->flush();

        $jsonContent = $serializer->serialize($viajeroEnviado, 'json');
        return new Response($jsonContent);
    }
}
