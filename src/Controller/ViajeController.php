<?php

namespace App\Controller;

use App\Entity\Viaje;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;


class ViajeController extends AbstractController
{


    /**
     * @Route("/viaje/{id}", name="viaje_show")
     */
    public function show(Viaje $viaje)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];


        $serializer = new Serializer($normalizers, $encoders); 
        $jsonContent = $serializer->serialize($viaje, 'json');
        return new Response($jsonContent);
    }












    /**
     * @Route("/viaje/edit/{id}", methods={"POST","HEAD"})
     */
    public function update(Request $request, $id)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        
        $serializer = new Serializer($normalizers, $encoders);  
        $data = $request->getContent();

        $viajeEnviado = $serializer->deserialize($data, Viaje::class, 'json');
        $entityManager = $this->getDoctrine()->getManager();
        $viaje = $entityManager->getRepository(Viaje::class)->find($id);

        if (!$viaje) {
            throw $this->createNotFoundException(
                'No viaje found for id '.$id
            );
        }

        $viaje->setCodigo($viajeEnviado->getCodigo());
        $viaje->setPlazas($viajeEnviado->getPlazas());
        $viaje->setDestino($viajeEnviado->getDestino());
        $viaje->setOrigen($viajeEnviado->getOrigen());
        $viaje->setPrecio($viajeEnviado->getPrecio());
        $entityManager->flush();

        $jsonContent = $serializer->serialize($viaje, 'json');
        return new Response($jsonContent);
    }









    /**
     * @Route("/viaje/new/", methods={"POST","HEAD"})
     */
    public function new(SerializerInterface $serializer, Request $request)
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders); 

        $data = $request->getContent();
        $viajeEnviado = $serializer->deserialize($data, Viaje::class, 'json');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($viajeEnviado);
        $entityManager->flush();

        $jsonContent = $serializer->serialize($viajeEnviado, 'json');
        return new Response($jsonContent);
    }
}
