<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController
{
     /**
      * @Route("/lucky/numberx")
      */
    public function number()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }

    public function letter()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky letter: '.$number.'</body></html>'
        );
    }
}

?>