<?php

namespace App\Controller;

use App\Entity\Reserva;
use App\Entity\Viaje;
use App\Entity\Viajero;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;


class ReservaController extends AbstractController
{

    /**
     * @Route("/reserva/{id}", name="reserva_show")
     */
    public function show(Reserva $reserva)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders); 
        $jsonContent = $serializer->serialize($reserva, 'json');
        return new Response($jsonContent);
    }



    /**
     * @Route("/reserva/delete/{id}")
     */
    public function delete(Reserva $reserva)
    {
        $entityManager = $this->getDoctrine()->getManager();        
        $entityManager->remove($reserva);
        $entityManager->flush();
        return new Response('{}');
    }





    /**
     * @Route("/reserva/edit/{id}", methods={"POST","HEAD"})
     */
    public function update(Request $request, $id)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        
        $serializer = new Serializer($normalizers, $encoders);  
        $data = $request->getContent();

        $reservaEnviado = $serializer->deserialize($data, Reserva::class, 'json');
        $entityManager = $this->getDoctrine()->getManager();
        $reserva = $entityManager->getRepository(Reserva::class)->find($id);

        if (!$reserva) {
            throw $this->createNotFoundException(
                'No reserva found for id '.$id
            );
        }

        $reserva->setCodigo($reservaEnviado->getCodigo());
        $reserva->setPlazas($reservaEnviado->getPlazas());
        $reserva->setDestino($reservaEnviado->getDestino());
        $reserva->setOrigen($reservaEnviado->getOrigen());
        $reserva->setPrecio($reservaEnviado->getPrecio());
        $entityManager->flush();

        $jsonContent = $serializer->serialize($reserva, 'json');
        return new Response($jsonContent);
    }







    /**
     * @Route("/reserva/new/idViaje/{idviaje}/idViajero/{idviajero}", methods={"POST","HEAD"})
     */
    public function new(SerializerInterface $serializer, Request $request, $idviaje, $idviajero)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $viaje = $entityManager->getRepository(Viaje::class)->find($idviaje);
        $viajero = $entityManager->getRepository(Viajero::class)->find($idviajero);

        $reserva = new Reserva();
        $reserva->setViaje($viaje);
        $reserva->setViajero($viajero);
        $entityManager->persist($reserva);
        $entityManager->flush();

        $jsonContent = $serializer->serialize($reserva, 'json');        
        return new Response($jsonContent);
    }


}